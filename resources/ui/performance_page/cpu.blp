/* ui/performance_page/cpu.blp
 *
 * Copyright 2023 Romeo Calota
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Gtk 4.0;

template $PerformancePageCpu : Box {
  orientation: vertical;

  WindowHandle {
    child: Box description {
      orientation: vertical;
      spacing: 7;
      hexpand: true;

      Box {
        spacing: 20;

        Label {
          styles [
            "title-1",
          ]

          hexpand: true;
          halign: start;
          label: _("CPU");
        }

        Label cpu_name {
          styles [
            "title-3",
          ]

          halign: end;
          ellipsize: middle;
        }
      }

      Box {
        Label utilization_label_all {
          styles [
            "caption",
          ]

          hexpand: true;
          halign: start;

          visible: false;
        }

        Label utilization_label_overall {
          styles [
            "caption",
          ]

          hexpand: true;
          halign: start;
          label: _("% Utilization");

          visible: false;
        }

        Label {
          styles [
            "caption",
          ]

          label: _("100%");
        }
      }
    };
  }

  Grid usage_graphs {
    row-spacing: 7;
    column-spacing: 7;
    vexpand: true;
    hexpand: true;

    height-request: 120;

    row-homogeneous: true;
    column-homogeneous: true;
  }

  Box {
    margin-top: 2;
    height-request: 25;

    Box overall_graph_labels {
      visible: false;

      Label graph_max_duration {
        styles [
          "caption",
        ]

        hexpand: true;
        halign: start;
        valign: start;
      }

      Label {
        styles [
          "caption",
        ]

        valign: start;
        label: "0";
      }
    }
  }

  Box details {
    spacing: 20;

    visible: bind template.summary-mode inverted;

    Box dynamic_data {
      orientation: vertical;
      spacing: 10;

      Box top_row {
        spacing: 15;

        Box {
          orientation: vertical;
          spacing: 3;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Utilization");
          }

          Label utilization {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }

        Box {
          orientation: vertical;
          spacing: 3;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Speed");
          }

          Label speed {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }
      }

      Box mid_row {
        spacing: 15;
        width-request: 200;

        Box {
          orientation: vertical;
          spacing: 3;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Processes");
          }

          Label processes {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }

        Box {
          orientation: vertical;
          spacing: 3;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Threads");
          }

          Label threads {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }

        Box {
          orientation: vertical;
          spacing: 3;

          Label {
            styles [
              "caption",
            ]

            halign: start;
            label: _("Handles");
          }

          Label handles {
            styles [
              "title-4",
            ]

            halign: start;
          }
        }
      }

      Box {
        orientation: vertical;
        spacing: 3;

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Up time");
        }

        Label uptime {
          styles [
            "title-4",
          ]

          halign: start;
        }
      }
    }

    Box system_info {
      spacing: 10;

      Box labels {
        orientation: vertical;
        spacing: 3;

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Base Speed:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Sockets:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Virtual processors:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Virtualization:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("Virtual machine:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("L1 cache:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("L2 cache:");
        }

        Label {
          styles [
            "caption",
          ]

          halign: start;
          label: _("L3 cache:");
        }
      }

      Box values {
        orientation: vertical;
        spacing: 3;

        Label base_speed {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label sockets {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label virt_proc {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label virtualization {
          styles [
            "caption",
          ]

          halign: start;
          label: "Unknown";
        }

        Label virt_machine {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label l1_cache {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label l2_cache {
          styles [
            "caption",
          ]

          halign: start;
        }

        Label l3_cache {
          styles [
            "caption",
          ]

          halign: start;
        }
      }
    }
  }

  PopoverMenu context_menu {
    has-arrow: false;
    menu-model: context_menu_model;
  }
}

menu context_menu_model {
  section {
    submenu {
      label: _("Change G_raph To");

      item {
        label: _("Overall U_tilization");
        action: "graph.overall";
      }

      item {
        label: _("Logical _Processors");
        action: "graph.all-processors";
      }
    }
  }

  section {
    item {
      label: _("Graph _Summary View");
      action: "graph.summary";
    }

    submenu {
      label: _("_View");

      item {
        label: _("CP_U");
        action: "graph.cpu";
      }

      item {
        label: _("_Memory");
        action: "graph.memory";
      }

      item {
        label: _("_Disk");
        action: "graph.disk";
      }

      item {
        label: _("_Network");
        action: "graph.network";
      }

      item {
        label: _("_GPU");
        action: "graph.gpu";
      }
    }
  }

  section {
    item {
      label: _("_Copy");
      action: "graph.copy";
    }
  }
}
