/* ui/performance_page/page.blp
 *
 * Copyright 2023 Romeo Calota
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

using Gtk 4.0;

template $PerformancePage : Box {
  orientation: vertical;

  Box page_content {
    margin-top: 10;
    margin-bottom: 15;
    margin-end: 20;

    ScrolledWindow {
      visible: bind template.summary-mode inverted;
      width-request: 220;

      hscrollbar-policy: never;

      ListBox sidebar {
        styles [
          "navigation-sidebar",
        ]
      }
    }

    Stack page_stack {
      margin-start: 20;
      transition-type: slide_up_down;
    }
  }
}
